from base import *  # noqa


DEBUG = True
TEMPLATE_DEBUG = DEBUG


# Debug & DebugToolbar ########################################################
MIDDLEWARE_CLASSES += ('debug_toolbar.middleware.DebugToolbarMiddleware', )
INSTALLED_APPS += ('debug_toolbar', )

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'debug_toolbar.middleware.show_toolbar',
    'INTERCEPT_REDIRECTS': False,
}


# Map Publishing ##############################################################
MAP_PUBLISH_ROOT_URL = "http://localhost:8000/maps/"
